// add storage event Listener
import store from './store/store'
import router from './router'

// add listener
window.addEventListener("storage", checkStorage);

function checkStorage (event) {
  // check has localstorage 
  if (localStorage.getItem('vuex') != null) {
    const userToken = JSON.parse(event.storageArea.vuex);
    if(userToken.user.idToken == null) {
      store.state.user.idToken = null
      store.state.user.userId = null
      store.state.user.user = null
      router.push('/login')
    } else {
      const storage = JSON.parse(event.storageArea.vuex);
      const data = {
        token: storage.user.idToken,
        userId: storage.user.userId,
        userEmail: storage.user.user
      }
      store.commit('authUser', data)
      router.push('/')
    }
  } else {
      store.state.user.idToken = null
      store.state.user.userId = null
      store.state.user.user = null
      router.push('/login')
  }
}