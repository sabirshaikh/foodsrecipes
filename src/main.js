// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import scrollTop from '@/components/ui/btn-scroll-top'
import breadcrumbs from '@/components/common/breadcrumbs'
import alertBox from '@/components/ui/alertBox'
import '../node_modules/nprogress/nprogress.css'
import Vuelidate from 'vuelidate'
import store from './store/store'
import VueIziToast from 'vue-izitoast';
import 'izitoast/dist/css/iziToast.css';
import './local-storage'
const toastOption = {
    position: "topCenter",
    timeout: 3000,
}
Vue.use(VueIziToast, toastOption);

Vue.use(Vuelidate)
Vue.component('btn-scroll-top', scrollTop)
Vue.component('breadcrumbs', breadcrumbs)
Vue.component('alert-box', alertBox)
/* eslint-disable no-new */


new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
