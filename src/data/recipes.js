const recipes = [
	{recipeName: 'recipe 1', image: '../../../static/images/img8.jpg', difficulty: 'medium', likes: 10, comments: 5, recipeLink: '/recipeDetails/1'},
	{recipeName: 'recipe 2', image: '../../../static/images/img2.jpg', difficulty: 'hard', likes: 13, comments: 2, recipeLink: '/recipeDetails/2'},
	{recipeName: 'recipe 3', image: '../../../static/images/img3.jpg', difficulty: 'easy', likes: 4, comments: 4, recipeLink: '/recipeDetails/3'},
	{recipeName: 'recipe 4', image: '../../../static/images/img4.jpg', difficulty: 'easy', likes: 45, comments: 1, recipeLink: '/recipeDetails/4'},
	{recipeName: 'recipe 5', image: '../../../static/images/img5.jpg', difficulty: 'medium', likes: 43, comments: 6, recipeLink: '/recipeDetails/5'},
	{recipeName: 'recipe 6', image: '../../../static/images/img6.jpg', difficulty: 'hard', likes: 5, comments: 8, recipeLink: '/recipeDetails/6'},
]
export default recipes