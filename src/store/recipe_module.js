import axios from '../api/recipe-api'

const recipe = {
  state: {
    recipes: null
  },
  mutations: {
    setRecipe(state, data) {
      state.recipes = data
    }
  },
  actions: {
    postRecipe ({commit}, recipeData) {
      try {
        return axios.post('/recipes.json' + '?auth=' + this.state.user.idToken, recipeData)
          .then(res => {
            if (res.status === 200) {
              return { msg: 'Registration successfull', status: 200 }
            }
          })
          .catch(error => {
            console.log('error post recipe1:', error.response)
            if (error.response.data.status === 401) {
              return { msg: 'Unauthorized user', status: 401 }
            }
            return false
          })
      } catch (error) {
        console.log('error post recipe2:', error.response)
        return false
      }
      
    },
    getRecipes ({commit}) {
      return axios.get('/recipes.json')
        .then(res => {
          if (res.status === 200) {
            commit('setRecipe', res)
            return res.data
          }
        })
        .catch(error => {
          console.log('error post recipe:', error.response)
          // if (error.data.status === 401) {
          //   return { msg: 'Unauthorized user', status: 401 }
          // }
          return false
        })
    },
    getRecipeDetails ({commit}, key) {
      return axios.get('/recipes/' + key + '.json') 
        .then(res => {
          if (res.status === 200) {
            return res.data
          }
        })
        .catch(error => {
          console.log('error post recipe:', error.response)
          // if (error.data.status === 401) {
          //   return { msg: 'Unauthorized user', status: 401 }
          // }
          return false
        })
    }
  },
  getters: {
    getRecipesCount (state) {
      if (state.recipes != null) {
        return Object.keys(state.recipes.data).length
      } else {
        return 0;
      }
    }
  }
}

export default recipe
