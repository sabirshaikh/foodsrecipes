import Vue from 'vue'
import Vuex from 'vuex'
import user from './user_module'
import recipe from './recipe_module'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  namespace: true,
  modules: {
    user,
    recipe
  }
})
