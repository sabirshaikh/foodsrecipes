import router from '../router/index'
import axios from '../api/axios-auth'

const user = {
  state: {
    idToken: null,
    userId: null,
    user: null
  },
  mutations: {
    authUser (state, userData) {
      state.idToken = userData.token
      state.userId = userData.userId
      state.user = userData.userEmail
    },
    storeUser (state, user) {
      state.user = user
    },
    clearAuthData (state) {
      state.idToken = null
      state.userId = null
      state.user = null
      localStorage.removeItem('vuex');
    }
  },
  actions: {
    setLogoutTimer ({commit}, expirationTime) {
      commit('clearAuthData')
    },
    signup ({commit, dispatch}, authData) {
      try {
        return axios.post('/accounts:signUp?key='+ process.env.API_KEY, authData)
          .then(res => {
            if (res.status === 200) {
              return { msg: 'Registration successfull', status: 200 }
            }
          })
          .catch(error => {
            console.log('error registraion:', error.response)
            if(error.response.data.error.message === "EMAIL_EXISTS") {
              return { msg: 'Email already exists', status: 400 }
            }
            return false
          })
      }
      catch (error) {
        console.log('registration error msg:', error)
      }
    },
    login ({commit, dispatch}, authData) {
      try {
        return axios.post('/accounts:signInWithPassword?key='+ process.env.API_KEY, authData)
          .then(res => {
            console.log('res:', res)
            if (res.status === 200) {
              commit('authUser', {
                token: res.data.idToken,
                userId: res.data.localId,
                userEmail: res.data.email
              })
            }
            return true
          })
          .catch(error => {
            console.log('error axios:', error.response)
            return false
          })
      } catch (error) {
        console.log('error msg:', error)
      }
    },
    async logout ({commit, state}) {
      return new Promise((resolve, reject) => {
        commit('clearAuthData')
        if (state.idToken == null) {
          resolve(true)
        } else {
          reject(false)
        }
      })
    },
    storeUser ({commit, state}, userData) {
      if (!state.idToken) {
        return true
      }
    },
    fetchUser ({commit, state}) {
      if (!state.idToken) {
        return true
      }
    }
  },
  getters: {
    user (state) {
      return state.user
    },
    isAuthenticated (state) {
      return state.idToken !== null
    }
  }
}
export default user