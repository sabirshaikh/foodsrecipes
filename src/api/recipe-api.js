import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://vuejs-786.firebaseio.com'
})

// instance.defaults.headers.common['SOMETHING'] = 'something'

export default instance