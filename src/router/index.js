import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import recipes from '@/components/pages/recipes'
import errorPage from '@/components/pages/error'
import contact from '@/components/pages/contact'
import recipeDetails from '@/components/pages/recipe-details'
import registration from '@/components/pages/registration'
import login from '@/components/pages/login'
import search from '@/components/pages/search-recipes'
import submitRecipe from '@/components/pages/recipe-submit'
import myAccount from '@/components/pages/my-account'
import NProgress from 'nprogress'
import store from '@/store/store'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: home,
      meta: {
        title: 'Home | Recipes Chief'
      }
    },
    {
      path: '/recipes',
      name: 'Recipes',
      component: recipes,
      meta: {
        title: 'Recipes | Recipes Chief'
      }
    },
    {
      path: '/contact',
      alias: 'Contact_Us',
      name: 'Contact Us',
      component: contact,
      meta: {
        title: 'Contact | Recipes Chief'
      }
    },
    {
      path: '/recipeDetails/:id/:name',
      name: 'Recipe details',
      component: recipeDetails,
      meta: {
        title: 'recipe Details | Recipes Chief'
      }
    },
    {
      path: '/registration',
      name: 'Registration',
      component: registration,
      meta: {
        title: 'Registration | Recipes Chief'
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: login,
      meta: {
        title: 'Login | Recipes Chief'
      }
    },
    {
      path: '/search',
      name: 'search',
      component: search,
      meta: {
        title: 'Search for Recipes | Recipes Chief'
      }
    },
    {
      path: '/submit_recipe',
      name: 'Submit Recipe',
      component: submitRecipe,
      meta: {
        title: 'Submit Recipe | Recipes Chief',
        isAuth: true
      }
    },
    {
      path: '/my_account',
      name: 'My account',
      component: myAccount,
      meta: {
        title: 'My account | Recipes Chief',
        isAuth: true
      }
    },
    {
      path: '*',
      name: 'Error',
      component: errorPage,
      meta: {
        title: 'Page not found'
      }
    }
  ],
  linkExactActiveClass: 'current-menu-item',
  scrollBehavior (to, from, savedPosition) {
    // return desired position
    return { x: 0, y: 0 }
  }
})

router.beforeResolve((to, from, next) => {
  if (to.name) {
    NProgress.start()
  }
  next()
})

router.afterEach((to, from) => {
  NProgress.done()
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.isAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters.isAuthenticated) {
      return next('/login')
      // router.push('/login')
    }
  }

  if (to.meta.title != null) {
    document.title = to.meta.title
  } else {
    document.title = to.name
  }

  next()
})

export default router
