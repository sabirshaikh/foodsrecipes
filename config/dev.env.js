'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_KEY: '"AIzaSyBn-H7dbX8c_xVb57m0euQLGlO4KQfWDGA"'
})
